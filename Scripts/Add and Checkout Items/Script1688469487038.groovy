import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('D:\\katalon\\Android-MyDemoAppRN.1.2.0.build-231.apk', true)

Mobile.tap(findTestObject('Object Repository/android.widget.ImageView (16)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Add To Cart (2)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.ImageView (17)'), 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Proceed To Checkout (1)'), 0)

Mobile.sendKeys(findTestObject('Object Repository/android.widget.EditText'), 'bob@example.com')

Mobile.setEncryptedText(findTestObject('Object Repository/android.widget.EditText (1)'), 'dbXIBZ66cuht4KTRMDozZw==', 0)

Mobile.tap(findTestObject('Object Repository/android.widget.TextView - Login'), 0)

Mobile.setText(findTestObject('android.widget.EditText - Rebecca Winter'), 'Sora Nakagami', 0)

Mobile.setText(findTestObject('android.widget.EditText - Mandorley 112'), '765 Seaside Beach St.', 0)

Mobile.setText(findTestObject('android.widget.EditText - Truro'), 'Daytona Beach', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - 89750'), '32114', 0)

Mobile.setText(findTestObject('android.widget.EditText - United Kingdom'), 'United States', 0)

Mobile.tap(findTestObject('Object Repository/android.view.ViewGroup (4)'), 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - Rebecca Winter (2)'), 'Sora Nakagami', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - 3258 1265 7568 789'), '325812657568789', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - 0325'), '03/25', 0)

Mobile.setText(findTestObject('Object Repository/android.widget.EditText - 123'), '123', 0)

Mobile.tap(findTestObject('Object Repository/android.view.ViewGroup (5)'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/android.widget.TextView - Sauce Labs Backpack (2)'), 0)

Mobile.tap(findTestObject('Object Repository/android.view.ViewGroup (6)'), 0)

Mobile.verifyElementExist(findTestObject('Object Repository/android.widget.TextView - Checkout Complete'), 0)

Mobile.closeApplication()

